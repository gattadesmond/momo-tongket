/* eslint-disable */
const autoprefixer = require("gulp-autoprefixer"),
  browserSync = require("browser-sync"),
  cleanCSS = require("gulp-clean-css"),
  copyDepsYaml = "./copydeps.yml",
  cssImporter = require("node-sass-css-importer")({
    import_paths: ["./scss"]
  }),
  del = require("del"),
  eslint = require("gulp-eslint"),
  gulp = require("gulp"),
  log = require("fancy-log"),
  newer = require("gulp-newer"),
  path = require("path"),
  reload = browserSync.reload,
  rename = require("gulp-rename"),
  rollup = require("rollup"),
  rollupBabel = require("rollup-plugin-babel"),
  rollupCommonjs = require("rollup-plugin-commonjs"),
  rollupResolve = require("rollup-plugin-node-resolve"),
  rollupUglify = require("rollup-plugin-uglify").uglify,
  sass = require("gulp-sass"),
  sourcemaps = require("gulp-sourcemaps"),
  gulpif = require("gulp-if"),
  data = require('gulp-data');
  themeYaml = "./theme.yml",
  fs = require("fs"),
  year = new Date().getFullYear(),
  yaml = require("yamljs");

let copyDeps = yaml.load(copyDepsYaml);
let theme = yaml.load(themeYaml);

//Nunjuck render
const nunjucksRender = require("gulp-nunjucks-render");

const yargs = require("yargs");
// Check for --build flag
const PRODUCTION = !!yargs.argv.production;

const babelConfig = {
  presets: [
    [
      "@babel/env",
      {
        loose: true,
        modules: false,
        exclude: ["transform-typeof-symbol"]
      }
    ]
  ],
  plugins: ["@babel/plugin-proposal-object-rest-spread"],
  env: {
    test: {
      plugins: ["istanbul"]
    }
  },
  exclude: "node_modules/**", // Only transpile our source code
  externalHelpersWhitelist: [
    // Include only required helpers
    "defineProperties",
    "createClass",
    "inheritsLoose",
    "defineProperty",
    "objectSpread"
  ]
};

getPaths = () => {
  return {
    dist: "dest",
    html: {
      all: "src/html/**/*",
      html: "src/html/*.html",
      dataFile: "src/html/data/global.json",
      from: "src/html/*.html",
      folder: "src/html"
    },
    scss: {
      folder: "src/stylesheets",
      all: "src/stylesheets/**/*",
      root: "src/stylesheets/*.scss",
      include : "node_modules/bootstrap/scss",
      compatibility: ["last 2 versions", "ie >= 9"],
      themeScss: [
        "src/stylesheets/theme.scss",
      ]
    },
    assets: {
      all: "src/assets/**/*",
      folder: "src/assets",
      allFolders: [
        "src/assets/css",
        "src/assets/img",
        "src/assets/fonts",
        "src/assets/video"
      ]
    },
    css: {
      folder: "src/assets/css"
    },
    fonts: {
      folder: "src/assets/fonts",
      all: "src/assets/fonts/*.*"
    },
    images: {
      folder: "src/assets/img",
      all: "src/assets/img/*.*"
    },
    videos: {
      folder: "src/assets/video",
      all: "src/assets/video/*.*"
    },
    dist2: {
      packageFolder: "",
      folder: "dist",
      pages: "dist/pages",
      all: "dist/**/*",
      assets: "dist/assets",
      img: "dist/assets/img",
      css: "dist/assets/css",
      scssSources: "dist/scss",
      js: "dist/assets/js",
      jsSources: "dist/js",
      fonts: "dist/assets/fonts",
      video: "dist/assets/video",
      documentation: "dist/documentation",
      exclude: ["!**/desktop.ini", "!**/.DS_store"]
    },
    copyDependencies: copyDeps
  };
};
var paths = getPaths();

//If Build output to Public folder
if (PRODUCTION) {
  paths.dist = "public";
}

//DEFINE TASKS

//Delete the "dist" folder
//This happens every time a build start
gulp.task("clean:dist", function(done) {
  del.sync(paths.dist + "/**/*", {
    force: true
  });
  done();
});

/**
 * HTML Task
 */
gulp.task("html", () => {
  // get data for nunjucks templates
  function getData(file) {
    const data = JSON.parse(fs.readFileSync(paths.html.dataFile, "utf8"));
    data.file = file;
    data.ENV = process.env.NODE_ENV;
    data.filename = path.basename(file.path);
    // data.headerComment = getHeaderComment("html");

    // active menu item for menu
    data.isActiveMenuItem = function(itemFile, item, filename) {
      if (itemFile === filename || (item.sub && item.sub[filename])) {
        return true;
      }

      let returnVal = false;

      if (item.sub) {
        Object.keys(item.sub).forEach(fileSub => {
          const itemSub = item.sub[fileSub];

          if (fileSub === filename || (itemSub.sub && itemSub.sub[filename])) {
            returnVal = true;
          }
        });
      }

      return returnVal;
    };

    return data;
  }

  return gulp
    .src(paths.html.html)
    .pipe(data(getData))
    .pipe(
      nunjucksRender({
        path: paths.html.folder,
        envOptions: {
          watch: false
        }
      })
    )
    .pipe(gulp.dest(paths.dist))
    .on("end", () => {
      reload({
        stream: true
      });
    });
});

// Copy files out of the assets folder
// This task skips over the "img", "js", and "scss" folders, which are parsed separately

//SASS tasks
gulp.task("sass", () =>
  gulp
    .src(paths.scss.themeScss)
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        includePaths: paths.scss.include,
        importer: [cssImporter]
      }).on("error", sass.logError)
    )
    .pipe(
      autoprefixer()
    )
    .pipe(gulpif(!PRODUCTION, sourcemaps.write()))
    .pipe(
      gulpif(
        PRODUCTION,
        cleanCSS({
          compatibility: "ie9",
          level: {
            1: {
              specialComments: 0
            }
          }
        })
      )
    )
    .pipe(gulp.dest(paths.dist + "/css"))
    .pipe(
      browserSync.stream({
        match: "**/theme*.css"
      })
    )
);


// Assets
gulp.task("copy-assets", function() {
  return gulp
    .src(paths.assets.all, {
      base: paths.assets.folder
    })
    .pipe(newer(paths.dist))
    .pipe(gulp.dest(paths.dist))
    .pipe(
      reload({
        stream: true
      })
    );
});

gulp.task("deps", async done => {
  await paths.copyDependencies.forEach(function(filesObj) {
    let files;
    if (typeof filesObj.files == "object") {
      files = filesObj.files.map(file => {
        return `${filesObj.from}/${file}`;
      });
    } else {
      files = `${filesObj.from}/${filesObj.files}`;
    }

    gulp.src(files).pipe(gulp.dest(filesObj.to, { overwrite: true }));
  });
  done();
});

// watch files for changes and reload
gulp.task("serve", function(done) {
  browserSync({
    server: {
      baseDir: "./dest",
      index: "index.html"
    }
  });
  done();
});

gulp.task("watch", function(done) {
  // PAGES
  // Watch only .html pages as they can be recompiled individually
  gulp.watch(
    [paths.html.all],
    {
      cwd: "./"
    },
    gulp.series("html", function reloadPage(done) {
      reload();
      done();
    })
  );

  // SCSS
  // Any .scss file change will trigger a sass rebuild
  gulp.watch(
    [paths.scss.all],
    {
      cwd: "./"
    },
    gulp.series("sass")
  );


  done();
  // End watch task
});

gulp.task(
  "default",
  gulp.series(
    "clean:dist",
    "copy-assets",
    gulp.series("html", "sass"),
    gulp.series("serve", "watch")
  )
);

gulp.task(
  "build",
  gulp.series(
    "clean:dist",
    "copy-assets",
    gulp.series("html", "sass")
  )
);
